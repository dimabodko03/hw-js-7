const stringArr = ['hello', 'world', 23, '23', null];
let newArr = [];

function filterBy(arr, typeOfElement) {
    for (let i = 0; i < arr.length; i++) {
        if (typeof arr[i] !== typeOfElement) {
            newArr.push(stringArr[i]);
        }
    }
    return newArr;
}

console.log(filterBy(stringArr, 'string'));